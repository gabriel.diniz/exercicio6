import java.util.Scanner;

public class exercicio6 {

    public static void main(String[] args) { // método publico estatico principal

        exercicio6(); // método
    }

    public static void exercicio6() { // método

        Scanner in = new Scanner(System.in);

        int valor; // variavel

        System.out.println("Insira um valor: "); // inserir um valor
        valor = in.nextInt();

        System.out.println("O valor inserido é: " + valor); // imprimir valor

        System.out.println("O valor sucessor é: " + (valor + 1)); // imprimir valor sucessor

        System.out.println("O valor antecessor é: " + (valor - 1)); // imprimir valor antecessor

    }
}

